import React from 'react';
import logo from './logo.svg';
import './App.scss';

const App: React.FC = () => (
  <div className="container">
    <header className="row">
      <div className="col-xs-12 justify-content-center">
        <img src={logo} className="App-logo" alt="logo" />
      </div>
      <div className="col-xs-6">
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
      </div>
      <div className="col-xs-6">
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener 
            noreferrer"
          id="Link"
        >
          Learn React
        </a>
      </div>
    </header>
  </div>
);

export default App;
